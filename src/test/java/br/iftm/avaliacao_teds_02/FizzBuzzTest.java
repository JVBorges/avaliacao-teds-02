package br.iftm.avaliacao_teds_02;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FizzBuzzTest {

	private FizzBuzz fizzBuzz = new FizzBuzz();
	private String expectFizz = "Fizz";
	private String expectBuzz = "Buzz";
	private String expectFizzBuzz = "FizzBuzz";
	
	@Test
	void testFizz() {
		String result = fizzBuzz.calcular(9);
	    assertEquals(expectFizz, result);
	}
	
	@Test
	void testBuzz() {
		String result = fizzBuzz.calcular(20);
	    
	    assertEquals(expectBuzz, result);
	}
	
	@Test
	void testFizzBuzz() {
		String result = fizzBuzz.calcular(30);
	    
	    assertEquals(expectFizzBuzz, result);
	}
	
	@Test
	void testNumber() {
		String result = fizzBuzz.calcular(11);
	    
	    assertEquals("11", result);
	}
	
}
