package br.iftm.avaliacao_teds_02;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PraQueMesmoTest {
	
	private PraQueMesmo praQueMesmo = new PraQueMesmo();
	
	@Test
	void testNumeroMaiorQue30() {
		assertEquals(35 * 4, praQueMesmo.naoFacoIdeia(35));
	}
	
	@Test
	void testNumeroIgual30() {
		assertEquals(30 * 3, praQueMesmo.naoFacoIdeia(30));
	}
	
	@Test
	void testNumeroEntre30e10() {
		assertEquals(14 * 3, praQueMesmo.naoFacoIdeia(14));
	}
	
	@Test
	void testNumeroIgual10() {
		assertEquals(10 * 2, praQueMesmo.naoFacoIdeia(10));
	}
	
	@Test
	void testNumeroMenorQue10() {
		assertEquals(5 * 2, praQueMesmo.naoFacoIdeia(5));
	}

}
