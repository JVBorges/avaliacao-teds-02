package br.iftm.avaliacao_teds_02;

public class PraQueMesmo {
	public int naoFacoIdeia(int numero) {
        if (numero > 30) {
            return numero * 4;
        } else if (numero > 10) {
            return numero * 3;
        } else {
            return numero * 2;
        }
    }
}
